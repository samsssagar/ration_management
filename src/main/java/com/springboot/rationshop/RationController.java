package com.springboot.rationshop;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping("/api/ration")
public class RationController {

	@Autowired
	public RationRepo rationRepo;
	
	@GetMapping("/api/ration")
	public List<RationShop> listAllRationShops() {
		return this.rationRepo.findAll();
	}
	
	@PostMapping("/api/ration")
	public RationShop createRationShop(@RequestBody RationShop rationShop) {
		return this.rationRepo.save(rationShop);
	}
	
	@DeleteMapping("/api/ration")
	@Transactional
	public void deleteRationShop(@RequestParam long id) {
		this.rationRepo.deleteById(id);
	}
	
	@PutMapping("/api/ration")
	public RationShop updatedRationShop(@RequestParam long id, @RequestBody RationShop updatedRationShop) {
		RationShop rationShop = this.rationRepo.findById(id);
		rationShop.setName(updatedRationShop.getName());
		rationShop.setCity(updatedRationShop.getCity());
		rationShop.setState(updatedRationShop.getState());
		rationShop.setCountry(updatedRationShop.getCountry());
		rationShop.setAddress(updatedRationShop.getAddress());
		
		return this.rationRepo.save(rationShop);
	}
	
	@GetMapping("/api/ration/state")
	public List<RationShop> listRationShopsByState(@RequestParam String state){
		return this.rationRepo.findByState(state);
	}
	
	@GetMapping("/api/ration/revenue/state")
	public Long getRevenueByState(@RequestParam String state) {
		return this.rationRepo.findRevenueByState(state);
	}
	
}
