package com.springboot.rationshop;

import javax.persistence.Embeddable;

@Embeddable
public class RevenueModel {

	public Long revenue;
	
	

	public Long getRevenue() {
		return revenue;
	}



	public void setRevenue(Long revenue) {
		this.revenue = revenue;
	}



	public RevenueModel() {
	}
}
