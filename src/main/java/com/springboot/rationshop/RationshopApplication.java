package com.springboot.rationshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RationshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(RationshopApplication.class, args);
	}

}
