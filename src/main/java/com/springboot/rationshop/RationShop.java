package com.springboot.rationshop;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ration")
public class RationShop {
	
	@Id
	@GeneratedValue
	public long id;

	public String name;
	
	public String address;
	
	public String city;
	
	public String state;
	
	public String country;
	
	@Embedded
	public RevenueModel revenue;
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getState() {
		return state;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getCountry() {
		return country;
	}
	
	
	public RevenueModel getRevenue() {
		return revenue;
	}

	public void setRevenue(RevenueModel revenue) {
		this.revenue = revenue;
	}

	//for de-serialization
	public RationShop() {
	}
	
}
