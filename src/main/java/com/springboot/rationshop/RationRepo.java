package com.springboot.rationshop;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RationRepo extends JpaRepository<RationShop, Long>{
	
	public RationShop findById(long id);
	
	public List<RationShop> findByState(String state);
	
	public void deleteById(long id);
	
	//@Query("select " + "new com.springboot.rationshop.RationShop(r.revenue) " + "from RationShop r " + "group by " + "r.state")
	@Query("SELECT sum(r.revenue) FROM RationShop AS r GROUP BY r.state")
	public Long findRevenueByState(String state);
	
}
